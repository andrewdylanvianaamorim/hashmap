#include <stddef.h>
#include <stdlib.h>
#include <stdbool.h>

#ifndef HASHMAP_H
#define HASHMAP_H

struct HashMap {
    size_t length;
    size_t capacity;
    size_t key_size;
    size_t element_size;

    void *key_buffer;
    void *element_buffer;
    bool (*cmp_key)(void *key, void *another_key);
};

struct HashMap *create_hashmap(size_t key_size, size_t element_size, bool (*cmp_key)(void *, void*));
bool has_key(struct HashMap *map,void *key);
bool add_element(struct HashMap *map, void *key, void *element);
bool get_element(struct HashMap *map,void *key, void *element);
void *get_ref_element(struct HashMap *map, void *key);
bool delete_key(struct HashMap *map, void *key, void *deleted_element);
void delete_hashmap(struct HashMap **map);

#endif
