#include <assert.h>
#include <stdio.h>
#include <strings.h>
#include "HashMap.h"


struct Pessoa
{
    char *nome;
    int idade;
};

void add_element_teste()
{
    //Esse teste e feito para testar o dicionário com tipos primitivos
    puts("Iniciando teste 1");

    int key = 1;
    float elemento = 1.5f;

    struct HashMap *hs = create_hashmap(sizeof(int), sizeof(float), NULL);

    add_element(hs,&key, &elemento);

    assert(key == *(int *)hs->key_buffer);
    assert(elemento == *(float *)hs->element_buffer);

    //mudando o elemento(deve sobreescrever)
    elemento = 3.14f;
    add_element(hs, &key, &elemento);

    assert(key == *(int *)hs->key_buffer);
    assert(elemento == *(float *)hs->element_buffer);

    //inserindo uma nova chave
    key = 5;
    add_element(hs, &key, &elemento);
    assert(hs->length == 2);
    assert(key == *((int *)hs->key_buffer + 1));
    assert(elemento == *((float *)hs->element_buffer + 1));

    key++;
    add_element(hs, &key, &elemento);
    key++;
    add_element(hs, &key, &elemento);
    key++;
    add_element(hs, &key, &elemento);
    key++;
    add_element(hs, &key, &elemento);
    key++;
    add_element(hs, &key, &elemento);
    key++;
    add_element(hs, &key, &elemento);


    assert(key == *((int *)hs->key_buffer + hs->length - 1));
    assert(elemento == *((float *)hs->element_buffer + hs->length - 1));

    delete_hashmap(&hs);

    puts("Passou no teste 1");
}




bool cmp_pessoa(void *p1, void *p2)
{
    struct Pessoa *pessoa1 = (struct Pessoa *)p1;
    struct Pessoa *pessoa2 = (struct Pessoa *)p2;


    return
           strcasecmp(pessoa1->nome, pessoa2->nome) == 0;
}

bool str_cmp(void * str, void * str2)
{
    return strcasecmp(str,str2) == 0;
}

void add_element_teste_avancado()
{
    puts("Iniciando teste 2");

    struct Pessoa p1 = {.nome = "Andrew", .idade=19};
    struct Pessoa p2 = {.nome = "Lakitu", .idade=19};

    struct HashMap *hs = create_hashmap(sizeof(char **), sizeof(struct Pessoa), str_cmp);

    char *key = "Andrew";
    char *key2 = "Lakitu";

    add_element(hs, &key, &p1);

    assert(hs->length == 1);
    assert(strcasecmp(key, ((char **)hs->key_buffer)[0]) == 0);
    assert(cmp_pessoa(&p1, hs->element_buffer));

    p1.idade++;
    add_element(hs, &key, &p1);
    assert(hs->length == 1);
    assert(strcasecmp(key, ((char **)hs->key_buffer)[0]) == 0);
    assert(cmp_pessoa(&p1, hs->element_buffer));

    add_element(hs, &key2, &p2);
    assert(hs->length == 2);
    assert(str_cmp(key2, ((char **)hs->key_buffer)[1]));
    assert(cmp_pessoa(&p2, ((struct Pessoa *)hs->element_buffer) + 1));

    delete_hashmap(&hs);

    puts("Passou no teste 2");
}

void get_element_teste()
{
    puts("Iniciando teste 3");

    int key = 1;
    float elemento = 1.5f;

    struct HashMap *hs = create_hashmap(sizeof(int), sizeof(float), NULL);

    add_element(hs,&key, &elemento);

    float resultado;
    get_element(hs, &key, &resultado);
    assert(elemento == resultado);


    delete_hashmap(&hs);

    puts("Passou no teste 3");

}

void get_ref_element_teste() 
{
    puts("Iniciando teste 4");

    int chave = 0;
    int element = 1;

    struct HashMap *hs = create_hashmap(sizeof(chave), sizeof(element), NULL);

    add_element(hs, &chave, &element);

    void *ref = get_ref_element(hs, &chave);
    assert(*(int *)ref == element);

    chave = 90;
    ref = get_ref_element(hs, &chave);
    assert(ref == NULL);

    delete_hashmap(&hs);

    puts("Passou no teste 4");
}

void delete_key_teste() {
    puts("Iniciando teste 5");
    struct HashMap *hs = create_hashmap(sizeof(int), sizeof(int), NULL);
    for (int i = 0; i < 10; i++)
    {
        add_element(hs, &i, &i);
    }

    int chave = 4;

    int deleted_element;
    int backup_length = hs->length;

    delete_key(hs, &chave, &deleted_element);

    assert(deleted_element == chave);
    assert(hs->length == backup_length - 1);
    assert(5 == *((int *)hs->element_buffer + chave));

    delete_hashmap(&hs);

    puts("Passou no teste 5");

}

int main(void) {
    add_element_teste();
    add_element_teste_avancado();
    get_element_teste();
    get_ref_element_teste();
    delete_key_teste();
    return 0;
}
