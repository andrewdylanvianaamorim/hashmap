#include "HashMap.h"
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>

bool default_cmp_key(void *key1, void *key2, size_t key_size) {
    return memcmp(key1, key2, key_size) == 0;
}

bool grow_map(struct HashMap *map)
{
    size_t grow_factor = 2;
    void *new_key_buffer = realloc(map->key_buffer,map->key_size * (map->capacity *grow_factor));
    void *new_element_buffer = realloc(map->element_buffer,map->element_size * (map->capacity *grow_factor));
    if (new_element_buffer == NULL || new_key_buffer== NULL) {
        return false;
    }
    map->element_buffer = new_element_buffer;
    map->key_buffer = new_key_buffer;
    map->capacity *= grow_factor;
    return true;
}

struct HashMap *create_hashmap(size_t key_size, size_t element_size, bool (*cmp_key)(void *, void*)) {
    size_t capacity = 8;
    void *key_buffer = malloc(key_size * capacity);
    void *element_buffer = malloc(element_size * capacity);

    struct HashMap *map = (struct HashMap *)malloc(sizeof(struct HashMap));

    if (map == NULL) return map;

    *map = (struct HashMap){
        .length = 0,
        .capacity = capacity,
        .key_size = key_size,
        .element_size = element_size,
        .key_buffer = key_buffer,
        .element_buffer = element_buffer,
        .cmp_key = cmp_key
    };

    return map;
}
/*
    adiciona elemento ou sobrescreve caso a chave já tenha um elemento
    se não tiver a chave ela será criada
 */
bool add_element(struct HashMap *map, void *key, void *element)
{
    if (map->length == map->capacity - 1) 
    {
        if (!grow_map(map)) return false;
    }

    size_t key_index = 0;

    if (has_key(map, key)) 
    {        
        for (size_t i = 0; i < map->length; i++) 
        {
            bool is_key_equal = false;
            void* k = (void*)((char*)map->key_buffer + i * map->key_size);
            if (map->cmp_key == NULL) 
            {
                is_key_equal = default_cmp_key(k, key, map->key_size);
            }
            else
            {
                is_key_equal = map->cmp_key(k, key);
            }

            if (is_key_equal) 
            {
                key_index = i;
            }
        }

    }
    else 
    {
        key_index = map->length;
    }



    void *next_key = (void *)((char *)map->key_buffer + map->key_size * key_index);
    void *next_element = (void *)((char *)map->element_buffer + map->element_size * key_index);

    memcpy(next_key, key, map->key_size);
    memcpy(next_element, element, map->element_size);

    //Só incrementamos quando um uma key nova é adicionada
    if (key_index == map->length)
        map->length++;
    return true;
}


bool has_key(struct HashMap *map,void *key)
{
    char *k = map->key_buffer;
    int i = 0;
    for (; i < map->length;k += map->key_size,i++) {
        bool resultado = false;
        if (map->cmp_key == NULL)
        {
            resultado = default_cmp_key(k, key, map->key_size);
        }
        else
        {
            resultado = map->cmp_key(k, key);
        }
        if (resultado) return true;
    }
    return false;
}


bool get_element(struct HashMap *map,void *key, void *element)
{
    if (has_key(map, key))
    {
        size_t index;
        for (index = 0; index < map->length; index++)
        {
            bool resultado = false;
            if (map->cmp_key == NULL)
            {
                resultado = default_cmp_key(key, map->key_buffer + index * map->key_size, map->key_size);
            }
            else
            {
                resultado = map->cmp_key( map->key_buffer + index * map->key_size, key);
            }

            if (resultado) break;
        }
        memcpy(element, map->element_buffer + (map->element_size * index), map->element_size);
        return true;
    }
    else
    {
        return false;
    }
}

void  delete_hashmap(struct HashMap **map)
{
    free((*map)->key_buffer);
    free((*map)->element_buffer);
    free(*map);
    *map = NULL;
}


void *get_ref_element(struct HashMap *map, void *key)
{
    if (!has_key(map, key)) return NULL;
    size_t index;
    for (index = 0; index < map->length; index++)
    {
        bool resultado = false;
        if (map->cmp_key == NULL)
        {
            resultado = default_cmp_key(key, map->key_buffer + index * map->key_size, map->key_size);
        }
        else
        {
            resultado = map->cmp_key( map->key_buffer + index * map->key_size, key);
        }

        if (resultado) break;
    }
    return map->element_buffer + (index * map->element_size);
}

// também deleta o elemento
// deve verificar se a chave existe
// existindo deve salvar o dado há ser excluido, descer todos os elementos para a posição correspondente a chave
// então deve se sobreescrever o o parametro: deleted_element com o valor salvo, se deleted_element != NULL
// caso não tenha a chave o deleted_element deverá ser sobreescrito com null e a função retornará false;
bool delete_key(struct HashMap *map, void *key, void *deleted_element)
{
    if (!has_key(map, key))
    {
        deleted_element = NULL;
        return false;
    }

    size_t index;
    for (index = 0; index < map->length; index++)
    {
        bool resultado = false;
        if (map->cmp_key == NULL)
        {
            resultado = default_cmp_key(key, map->key_buffer + index * map->key_size, map->key_size);
        }
        else
        {
            resultado = map->cmp_key( map->key_buffer + index * map->key_size, key);
        }

        if (resultado) break;
    }

    if (deleted_element != NULL)
    {
        memcpy(deleted_element, map->element_buffer + (map->element_size * index), map->element_size);
    }


    memcpy(
        map->key_buffer + (map->key_size * index), 
        map->key_buffer + (map->key_size * (index + 1)), 
        (map->length - (index + 1)) * map->key_size);
        
    memcpy(
        map->element_buffer + (map->element_size * index), 
        map->element_buffer + (map->element_size * (index + 1)), 
        (map->length - (index + 1)) * map->element_size);
    map->length--;
    return true;
}
